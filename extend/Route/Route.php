<?php

namespace package\Route;
use Closure;
use \think\facade\Route as RouteAll;

/**
 * Class Route
 * @package package\Route
 * @method static \think\route\RuleItem get(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @method static \think\route\RuleItem post(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @method  static \think\route\RuleItem put(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @method \think\route\RuleItem delete(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @method static \think\route\RuleItem patch(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @method \think\route\RuleItem any(string $rule, mixed $route, array $option = [], array $pattern = [])
 * @see \think\Route
 */
class Route extends RouteAll
{

    protected static $arrayCallableToActionMethods = ['get', 'post', 'put', 'delete', 'patch', 'any'];

    /**
     * @param  string  $method
     * @param  array   $args
     * @return mixed
     */
    public static function __callStatic($method, $args){
        if (in_array($method, static::$arrayCallableToActionMethods) && isset($args[1]) && is_array($args[1])){
            if (isset($args[1]['uses'])){
                $args[1]['uses'] = static::arrayCallableToAction($args[1]['uses']);
            }else{
                $args[1] = static::arrayCallableToAction($args[1]);
            }
        }elseif($method==='group'){
            if (count($args)===2 && isset($args[1]) && is_array($args[1]) && count($args[1]) === 2 && isset($args[1][1])){
                if (method_exists($args[1][0], $args[1][1])){
                    $fn = $args[1];
                    $args[1] = Closure::bind(function () use (&$fn){
                        return call_user_func_array($fn, func_get_args());
                    }, null, $args[1][0]);
                    unset($fn);
                }
            }
        }
        return parent::__callStatic($method, $args);
    }

    /**
     * @param callable $hander
     * @return string
     */
    public static function arrayCallableToAction($hander){
        if(is_array($hander) && count($hander) === 2 && isset($hander[1])){
            list($className, $methodName) = $hander;
            if (method_exists($className, $methodName)){
                $hander = $className.'/'.$methodName;
//                $hander = $className.'@'.$methodName;
            }
            unset($className, $methodName);
        }
        return $hander;
    }






}