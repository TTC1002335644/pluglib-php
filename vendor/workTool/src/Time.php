<?php
namespace workTool\src;
use workTool\src\Interfaces\Time as TimeInterfaces;

class Time implements  TimeInterfaces{

    /**
     * 一天的秒数
     */
    const DAY = 86400;

    /**
     * 一周的秒数
     */
    const WEEk = 604800;



    /**
     * 今日开始和结束的时间戳
     * @return mixed
     */
    public static function today() : array {
        return self::getOneDayTime(0);
    }

    /**
     * 昨日开始和结束的时间戳
     * @return mixed
     */
    public static function yesterday(){
        return self::getOneDayTime(-1);
    }



    /**
     * 本周开始和结束的时间戳
     * @return mixed
     */
    public static function week(){
        return self::getOneWeekTime();
    }

    /**
     * 上周开始和结束的时间戳
     * @return mixed
     */
    public static function lastWeek(){
        return self::getOneWeekTime(-1);
    }



    /**
     * 本月开始和结束的时间戳
     * @return mixed
     */
    public static function month(){
        return self::getOneMonthTime();
    }

    /**
     * 上月开始和结束的时间戳
     * @return mixed
     */
    public static function lastMonth(){
        return self::getOneMonthTime(-1);
    }



    /**
     * 今年开始和结束的时间戳
     * @return mixed
     */
    public static function year(){
        return self::getOneYearTime();
    }

    /**
     * 去年开始和结束的时间戳
     * @return mixed
     */
    public static function lastYear(){
        return self::getOneYearTime(-1);
    }


    /**
     * 获取多少天前零点到现在的时间戳
     * @return mixed
     */
    public static function dayToNow(int $count = 1 , bool $isYesterday = false) : array{
        $timeStr = $count.' day';
        $start = self::getOneDayTime($count)[0];
        $end = ($isYesterday == false) ? time() : self::getOneDayTime(-1)[1];
        return [$start,$end];
    }

    /**
     * 获取多少天前的时间戳
     * @return mixed
     */
    public static function daysAgo(int $count = 0){
        return self::getOneDayTime(-1 * $count);
    }

    /**
     * 获取多少天后的时间戳
     * @return mixed
     */
    public static function daysAfter(int $count = 0){
        return self::getOneDayTime($count);
    }

    /**
     * 天数转换成秒数
     * @return mixed
     */
    public static function daysToSecond(int $count = 0){
        return $count * self::DAY;
    }

    /**
     * 周数转换成秒数
     * @return mixed
     */
    public static function weekToSecond(){
        return $count * self::WEEk;
    }


    /**
     * 获取某个步长的一天的时间
     * @param int $dayNum
     * @return array
     *
     */
    public static function getOneDayTime(int $count = 0):array {
        if($count == 0){
            $start = strtotime(date(    'Ymd 00:00:00'));
        }else{
            $timeStr = $count.' day';
            $start = strtotime(date(    'Ymd 00:00:00' , strtotime($timeStr)));
        }
        $end = $start + self::DAY - 1;
        return [$start,$end];
    }

    /**
     * 获取某个步长的一周的时间
     * @param int $count
     * @return array
     */
    public static function getOneWeekTime(int $count = 0) : array{
        if($count == 0){
            $start = strtotime(date('Y-m-d 00:00:00',strtotime('this week')));
        }else{
            $count = $count > 0 ? $count : '+'.$count;
            $timeStr = $count.' week';
            $start = strtotime(date('Y-m-d 00:00:00',strtotime($timeStr)));
        }
        $end = $start + self::WEEk - 1;
        return [$start,$end];
    }


    /**
     * 获取某个步长的一月的时间
     * @param int $conut
     * @return array
     */
    public static function getOneMonthTime(int $count = 0){
        if($count == 0){
            $start = strtotime(date('Y-m-01 00:00:00'));
        }else{
            $timeStr = $count.' month';
            $start = strtotime(date('Y-m-01 00:00:00' , strtotime($timeStr)));
        }
        $end = strtotime('+ 1 month' , $start) -1;
        return [$start,$end];
    }



    /**
     * 获取某个步长的一年的时间
     * @param int $conut
     * @return array
     */
    public static function getOneYearTime(int $count = 0){
        if($count == 0){
            $start = strtotime(date('Y-01-01 00:00:00'));
        }else{
            $timeStr = $count.' year';
            $start = strtotime(date('Y-01-01 00:00:00' , strtotime($timeStr)));
        }
        $end = strtotime('+ 1 year' , $start) -1;
        return [$start,$end];
    }





}