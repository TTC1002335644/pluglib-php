<?php
namespace workTool\src\Interfaces;


interface Time{
    /**
     * 今日开始和结束的时间戳
     * @return mixed
     */
    public static function today();

    /**
     * 昨日开始和结束的时间戳
     * @return mixed
     */
    public static function yesterday();

    /**
     * 本周开始和结束的时间戳
     * @return mixed
     */
    public static function week();

    /**
     * 上周开始和结束的时间戳
     * @return mixed
     */
    public static function lastWeek();

    /**
     * 本月开始和结束的时间戳
     * @return mixed
     */
    public static function month();

    /**
     * 上月开始和结束的时间戳
     * @return mixed
     */
    public static function lastMonth();

    /**
     * 今年开始和结束的时间戳
     * @return mixed
     */
    public static function year();

    /**
     * 去年开始和结束的时间戳
     * @return mixed
     */
    public static function lastYear();

    /**
     * 获取多少天前零点到现在的时间戳
     * @return mixed
     */
    public static function dayToNow();

    /**
     * 获取多少天前的时间戳
     * @return mixed
     */
    public static function daysAgo();

    /**
     * 获取多少天后的时间戳
     * @return mixed
     */
    public static function daysAfter();

    /**
     * 天数转换成秒数
     * @return mixed
     */
    public static function daysToSecond();

    /**
     * 周数转换成秒数
     * @return mixed
     */
    public static function weekToSecond();


    /**
     * 获取某个步长的一天的时间
     * @return mixed
     */
    public static function getOneDayTime();

    /**
     * 获取某个步长的一天的时间
     * @return mixed
     */
    public static function getOneWeekTime();

    /**
     * 获取某个步长的一天的时间
     * @return mixed
     */
    public static function getOneMonthTime();

    /**
     * 获取某个步长的一天的时间
     * @return mixed
     */
    public static function getOneYearTime();




}