<?php
namespace workpackage\src\Libs\Http;
use \think\Request as thinkphpRequest;
use workpackage\src\Interfaces as WI;
use workpackage\src\Traits as WT;
use workpackage\core\Libs\Http\ParameterBag;


class Request extends thinkphpRequest implements 
    /**
     * 属性模块
     * */
    WI\Http\Request\Proper,
    /**
     * 逻辑模块
     */
    WI\Http\Request\Proyx,

    /**
     * 事务队列模块
     */
    WI\Http\Request\TranscationQuque,
    /**
     * 响应模块
     */
    WI\Http\Request\Response,
    /**
     * 验证模块
     */
    WI\Http\Request\Validate
{

    use WT\Http\Request\Proper;
    use WT\Http\Request\Proyx;
    use WT\Http\Request\TranscationQuque;
    use WT\Http\Request\Response;
    use WT\Http\Request\Validate;


    /**
     * @var
     */
    public $attributes;


    public function __construct(array $attributes = []){
        $this->initialize($attributes);
        parent::__construct();
    }

    /**
     * 初始化属性
     * @param $attributes
     */
    public function initialize($attributes){
        $this->attributes = new ParameterBag($attributes);
    }

    /**
     * 设置属性
     * @param $key
     * @param $value
     * @return $this
     */
    public function setAttribute($key , $value){
        if(is_string($key) || is_numeric($key)){
            $this->attributes->set($key , $value);
        }
        return $this;
    }

    /**
     * 批量设置属性
     * @param array $data
     * @return $this
     */
    public function setAttributes(array $data){
        if(!empty($data)){
            foreach ($data as $key => $value){
                $this->setAttribute($key , $value);
            }
        }
        return $this;
    }

    /**
     * 获取属性
     * @param $key
     * @return $this
     */
    public function getAttrbute($key){
        if(is_string($key) || is_numeric($key)){
            return $this->attributes->get($key);
        }
        return $this;
    }




}