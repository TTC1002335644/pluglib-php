<?php
namespace workpackage\src\Libs\Http\Request;

use workpackage\src\Exceptions\Http\ProxyException;
use workpackage\src\Interfaces\Http\Request\Proyx;

class ProxyLogic implements \workpackage\src\Interfaces\Http\Request\ProxyLogic {

    /**
     * @var \workpackage\src\Interfaces\Http\Request\Proyx
     */
    public $proxy;

    /**
     * @var string
     */
    protected $callClassName;

    /**
     * ProxyLogic constructor.
     * @param $callClassName
     * @param $proxy
     * @throws ProxyException
     * @throws \ReflectionException
     */
    public function __construct($callClassName , $proxy){
        if(!$proxy instanceof Proyx){
            throw new ProxyException(ProxyException::E_SELF_ERROR , ['message' => 'must a Proyx']);
        }
        $this->proxy = $proxy;
        $this->callClassName = $callClassName;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws ProxyException
     * @throws \ReflectionException
     */
    public function __call($name, $arguments){
        if(empty($this->callClassName) || empty($this->proxy)){
            throw new ProxyException(ProxyException::E_SELF_ERROR , ['message' => '调用环境不存在']);
        }
        $handler = [$this->callClassName , $name];

        if(!(is_callable($handler) || method_exists($this->callClassName , $name))){
            unset($handler , $name , $arguments);
            throw new ProxyException(ProxyException::E_SELF_ERROR , ['message' => '方法不存在']);
        }
        return $this->proxy->call($handler , $arguments , null , $this->callClassName);
    }

    /**
     * @param $name
     * @param $arguments
     * @throws ProxyException
     * @throws \ReflectionException
     */
    public static function __callStatic($name, $arguments){
        throw new ProxyException(ProxyException::E_SELF_ERROR , ['message' => '不支持静态调用，请使用->调用']);
    }


}