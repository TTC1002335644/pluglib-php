<?php
namespace workpackage\src\Model\Query;

use workpackage\src\Model\Query\Query;
use think\helper\Arr;
use think\helper\Str;

trait Builder{


    /**
     * @param $field
     * @param bool $except
     * @param string $tableName
     * @param string $prefix
     * @param string $alias
     * @return $this|mixed
     */
    public function field($field, $except = false, $tableName = '', $prefix = '', $alias = ''){
        if(is_string($field)){
            $field = explode(',' , $field);
        }
        if(is_array($field)){
            $field = Arr::arraySnake($field);
        }
        return parent::field($field , $except , $tableName , $prefix , $alias);
    }


}