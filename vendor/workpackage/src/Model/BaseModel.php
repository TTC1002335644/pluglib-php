<?php
namespace workpackage\src\Model;

use think\Model;
use think\helper\Arr;
use think\helper\Str;
use workpackage\src\Exceptions\Model\ModelException;
use workpackage\src\Interfaces\Http\Request\TranscationQuque;
use workpackage\src\Model\Query\Builder;
use workpackage\src\Model\Relation\RelationShip;
use workpackage\src\Model\Query\Query;


/**
 * Class BaseModel
 * @package workpackage\src\Model
 * @method Query field(mixed $field, boolean $except = false) static 指定查询字段
 */
class BaseModel extends Model{

    use Builder;

    public function getListsPage(){
        return parent::select();
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getAttribute($key){
        return parent::getAttr(Str::snake($key));
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $data
     */
    public function setAttribute($name, $value, $data = []){
        parent::setAttr(Str::snake($key) , $value , $data);
    }

    /**
     * @param array $data
     * @param bool $sync
     * @return $this|void
     */
    public function setAttributes(array $data = [] , $sync = false){
        if (empty($data)){
            return ;
        }

        foreach ($data as $key => $value){
            $this->setAttr($key , $value);
        }

        return $this;

    }

    /**
     * 返回驼峰的结果
     * @return array
     */
    public function toArray(){
        return Arr::indexCamel(parent::toArray());
    }


    /**
     * @param TranscationQuque $quque
     * @return BaseModel
     */
    public static function pushQuqueTransaction(TranscationQuque $quque){
        $model = new static();
        $quque->pushTranscationModel($model);
        return $model;
    }

    /**
     * 重载save方法
     * @param array $data
     * @param array $where
     * @param null $sequence
     * @return bool|void
     */
    public function save($data = [], $where = [], $sequence = null)
    {
        $this->force();
        return parent::save($data , $where , $sequence);
    }


    /**
     * 不强制更新
     * @param array $data
     * @param array $where
     * @param null $sequence
     * @return bool
     */
    public function notForceSave($data = [], $where = [], $sequence = null){
        return parent::save($data , $where , $sequence);
    }




}