<?php
namespace workpackage\src\Controller;
use workpackage\src\Libs\Http\Request;
use workpackage\src\Interfaces\Http\Request\Proyx;

/**
 * Class QuqueController
 * @package workpackage\src\Controller
 */
abstract class QuqueController extends BaseController {

    /**
     * @return mixed|\think\App|\think\facade\Request|Request
     */
    protected function createProyx(){
        if(function_exists('request')){
            return \request();
        } elseif (function_exists('app')){
            return app(Request::class);
        }else{
            return new Request();
        }
    }

    /**
     * @return mixed|\think\App|\think\facade\Request|Request
     */
    public function getProyx(){
        return $this->createProyx();
    }


}