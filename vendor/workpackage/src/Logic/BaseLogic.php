<?php
namespace workpackage\src\Logic;
use workpackage\src\Interfaces\Http\Request\Proyx;
use workpackage\src\Interfaces\Logic\BaseLogic as InterfaceBaseLogic;

class BaseLogic implements InterfaceBaseLogic{

    /**
     * @param Proyx $proyx
     * @return mixed
     */
    public static function proyx(Proyx $proyx){
        return $proyx->getProxyLogicByCallClass(get_called_class());
    }

}