<?php
namespace workpackage\src\Interfaces\Http\Request;

interface Proyx{

    /**
     * @param $handler
     * @return bool
     */
    public function isCallBack($handler) : bool ;

    /**
     * @param array|callable|\Closure|string $handler
     * @param array|\Easyke\Easycms\Interfaces\Common\Parameters $arguments
     * @param \workpackage\src\Interfaces\Common\Parameters $parameters
     * @param string|null $callCalssName
     * @return mixed
     */
    public function call($handler, $arguments = null, $parameters = null, $callCalssName = null);

    /**
     * @param $callClass
     * @return mixed
     */
    public function getProxyLogicByCallClass($callClass);


}