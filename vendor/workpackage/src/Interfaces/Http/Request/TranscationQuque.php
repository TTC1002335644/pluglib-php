<?php
namespace workpackage\src\Interfaces\Http\Request;
use workpackage\src\Interfaces\Http\Request\TranscationHook;


interface TranscationQuque extends TranscationHook
{
    /**
     * @param $transaction 队列的事务
     * @param $ifStart 是否启用
     * @return mixed
     */
    public function pushTranscation($transaction , $ifCallable);

    /**
     * 开始事务
     * @return mixed
     */
    public function beginTranscation();

    /**
     * 设置需要开启事务的model
     * @param $model
     * @return mixed
     */
    public function pushTranscationModel($model);

}