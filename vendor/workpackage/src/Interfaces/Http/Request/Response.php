<?php

namespace workpackage\src\Interfaces\Http\Request;

use Closure;

Interface Response
{
    /**
     * 强制替换
     */
    const RESPONSES_REPLACE = true;
    /**
     * 强制合并
     */
    const RESPONSES_ARRAY_MERGE = false;

    /**
     * @param $key
     * @param $value
     */
    public function setResponse($key, $value);

    /**
     * @return $this
     */
    public function clearResponse();

}