<?php
namespace workpackage\src\Interfaces\Http\Request;

interface TranscationHook
{
    /**
     * 监听提交
     * @param $handler
     * @return mixed
     */
    public function Commit($handler);

    /**
     * 监听结束
     * @param $handler
     * @return mixed
     */
    public function RollBack($handler);

    /**
     * 监听 Commit 或者 RollBack 之后触发
     * @param $handler
     * @return mixed
     */
    public function Completed($handler);

    /**
     * 清空所有事务的钩子
     * @param $handler
     * @return mixed
     */
    public function clearTranscationHooks();



}