<?php

namespace workpackage\src\Interfaces\Http\Request;

/**
 * Interface NextTransaction
 * @package workpackage\src\Interfaces\Http\Request
 */
interface NextTransaction
{

    /**
     * 调用下一个事物
     * @return bool
     */
    public function __invoke();

    /**
     * 调用下一个事物
     * @return bool
     */
    public function next();

}