<?php
/**
 * Created by PhpStorm.
 * User: 10023
 * Date: 2019/4/21
 * Time: 0:52
 */

namespace workpackage\src\Interfaces\Http\Request;


interface Proper
{
    /**
     * 获取属性
     * @param null $key
     * @param null $value
     * @return mixed
     */
    public function getProp($key = null , $value = null);

    /**
     * 设置属性
     * @param null $key
     * @param null $value
     * @return mixed
     */
    public function setProp($key = null , $value = null);

    /**
     * 复制属性
     * @param null $key
     * @param null $newKay
     * @return mixed
     */
    public function copyProp($key = null , $newKay = null);

    /**
     * 判断属性是否存在
     * @param $key
     * @return mixed
     */
    public function hasProp($key):bool;

    /**
     * 批量设置属性
     * @param array $dataSet
     * @param bool $isReplace
     * @return mixed
     */
    public function loadProp(array $data , bool $isReplace = false);



}