<?php
namespace workpackage\src\Interfaces\Logic;
use workpackage\src\Interfaces\Http\Request\Proyx;

interface BaseLogic{

    /**
     * @param Proyx $proyx
     * @return mixed
     */
    public static function proyx(Proyx $proyx);

}