<?php
namespace workpackage\src\Interfaces\Exceptions\ExceptionArray\Validate;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Validate\Validate;
final class ValidateLists
{
    const MESSAGES = [
        Validate::E_VALIDATE_ERROR => '{error}'
    ];

}