<?php
namespace workpackage\src\Interfaces\Exceptions\ExceptionArray\Common;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Common as EGC;


final class CommonLists{
    const MESSAGES = [
        EGC\Common::E_MUST_AN_ARRAY => '{name}必须是一个数组',
        EGC\Common::E_MUST_A_CALLABLE => '{name}必须是一个回调',
        EGC\Common::E_MUST_PARAM_KEY => '必须传入{key}这个参数',
        EGC\Common::E_MUST_PARAMS => '缺少{key}参数 . "\n" . {message}',
        EGC\Common::E_REFLECTION_EXCEPTION => '{message}',
        EGC\Common::E_PROTOTYPE_NOT_DEFINED => '存在一个{key}属性没有定义',
        EGC\Common::E_NOT_FIND_OBJECT => '没有找到{key}这个对象',
        EGC\Common::E_MUST_PARAM_ERROR => '参数错误',
        EGC\Common::E_SELF_ERROR => '{message}',
    ];


}