<?php
namespace workpackage\src\Interfaces\Exceptions\ExceptionArray\Model;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Model as ML;

final class ModelLists {

    const MESSAGES = [
        ML\Model::E_MODEL_ADD_FAIL => '{add}添加数据库失败异常',
        ML\Model::E_MODEL_SAVE_FAIL => '请求头{save}添加数据库失败异常',
        ML\Model::E_MODEL_UPDATE_FAIL => '{update}更新数据库失败异常',
        ML\Model::E_MODEL_DELETE_FAIL => '{delete}删除数据库失败异常',
        ML\Model::E_MODEL_QUERY_FAIL => '{query}查询数据库失败异常',
        ML\Model::E_MODEL_QUERY_EXISTS_FAIL => '数据{message}不存在',
        ML\Model::E_MODEL_QUERY_EXISTS => '数据已经存在',
        ML\Model::E_MODEL_DATA_EXCEPTION => '{message}异常',
        ML\Model::E_MODEL_DATA_INVALID => '{message}失效',
        ML\Model::E_MODEL_EXISTS => '模型已经存在',
        ML\Model::E_MODEL_NOT_EXISTS => '模型不存在',
        ML\Model::E_MODEL_DATA_EXISTS => '数据{data}已经存在',
        ML\Model::E_MODEL_CANNOT_DELETE => '数据{data}正在被{object}使用，不可删除',
    ];
}