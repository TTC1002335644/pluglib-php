<?php
namespace workpackage\src\Interfaces\Exceptions;

use http\Exception;
use workpackage\src\Interfaces\Http\Request\Response;
use workpackage\src\Interfaces\Exceptions\Core\ExceptionIds;


/**
 * 异常标准类型
 * Interface WorkException
 * @package workpackage\src\Interfaces\Exceptions
 */
interface CoreException extends ExceptionIds
{

    /**
     * 设置错误信息
     * @param string $message
     * @return mixed
     */
    public function setMessage(string $message);

    /**
     * 渲染
     * @param $request
     * @return mixed
     */
//    public function render($request) : \think\Response;
    public function render();



}