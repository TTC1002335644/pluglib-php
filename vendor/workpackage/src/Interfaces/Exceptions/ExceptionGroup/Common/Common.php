<?php
namespace workpackage\src\Interfaces\Exceptions\ExceptionGroup\Common;
use workpackage\src\Interfaces\Exceptions\CoreException;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup as EG;

interface Common extends CoreException , EG\Base\ExceptionPrefix
{

    /**
     * @example {name}必须是一个数组
     */
    const E_MUST_AN_ARRAY = 122001;
    /**
     * @example {name}必须是一个回调
     */
    const E_MUST_A_CALLABLE = 122002;
    /**
     * @example 必须传入{key}这个参数
     */
    const E_MUST_PARAM_KEY = 122003;
    /**
     * @example 缺少{key}参数 . "\n" . {message}
     */
    const E_MUST_PARAMS = 122004;
    /**
     * 中转RequestException 异常
     * @example {message}
     */
    const E_REFLECTION_EXCEPTION = 122005;
    /**
     * @example 存在一个{key}属性没有定义
     */
    const E_PROTOTYPE_NOT_DEFINED = 122006;

    /**
     * @example 没有找到{key}这个对象
     */
    const E_NOT_FIND_OBJECT = 122007;
    /**
     * @example 参数错误
     */
    const E_MUST_PARAM_ERROR = 123005;

    /**
     * @example {message}自定义的错误
     */
    const E_SELF_ERROR = 123009;


}