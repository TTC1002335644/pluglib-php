<?php

namespace workpackage\src\Interfaces\Exceptions\ExceptionGroup\Model;

use workpackage\src\Interfaces\Exceptions\CoreException;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup as EG;


/**
 * 模型基础异常
 * Interface Model
 * @package Easyke\Easycms\Interfaces\Exceptions\ExceptionIds\Base
 */
interface Model extends CoreException , EG\Base\ExceptionPrefix
{
    /**
     * @example {$add}添加数据库失败异常
     */
    const E_MODEL_ADD_FAIL = 151001;
    /**
     * @example 请求头{$save}添加数据库失败异常
     */
    const E_MODEL_SAVE_FAIL = 151002;
    /**
     * @example {$update}更新数据库失败异常
     */
    const E_MODEL_UPDATE_FAIL = 151003;
    /**
     * @example {$delete}删除数据库失败异常
     */
    const E_MODEL_DELETE_FAIL = 151004;
    /**
     * @example {$query}查询数据库失败异常
     */
    const E_MODEL_QUERY_FAIL = 151005;
    /**
     * @example 数据{$message}不存在
     */
    const E_MODEL_QUERY_EXISTS_FAIL = 151006;
    /**
     * @example 数据已经存在
     */
    const E_MODEL_QUERY_EXISTS = 151007;
    /**
     * @example {$message}异常
     */
    const E_MODEL_DATA_EXCEPTION= 151008;

    /**
     * @example {$message}失效
     */
    const E_MODEL_DATA_INVALID = 151009;
    /**
     * @example 模型已经存在
     */
    const E_MODEL_EXISTS = 151010;
    /**
     * @example 模型不存在
     */
    const E_MODEL_NOT_EXISTS = 151011;
    /**
     * @example 数据{$data}已经存在
     */
    const E_MODEL_DATA_EXISTS =151012;
    /**
     * @example 数据{$data}正在被{$object}使用，不可删除
     */
    const E_MODEL_CANNOT_DELETE =151013;
}
