<?php

namespace workpackage\src\Interfaces\Exceptions\ExceptionGroup\Validate;

use workpackage\src\Interfaces\Exceptions\CoreException;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup as EG;


/**
 * 模型基础异常
 * Interface Model
 * @package Easyke\Easycms\Interfaces\Exceptions\ExceptionIds\Base
 */
interface Validate extends CoreException , EG\Base\ExceptionPrefix
{
    /**
     * @example {error}
     */
    const E_VALIDATE_ERROR = 126000;

}
