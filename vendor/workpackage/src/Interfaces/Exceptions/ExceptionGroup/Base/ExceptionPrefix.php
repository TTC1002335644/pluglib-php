<?php
namespace workpackage\src\Interfaces\Exceptions\ExceptionGroup\Base;


/**
 * 异常的前缀
 * Interface ExceptionPrefix
 * @package workpackage\src\Interfaces\Exceptions\Base
 */
interface ExceptionPrefix
{
    /**
     * @example 异常的前缀
     */
    const EXCEPTION_ID_PREFIX = 'E_';


}