<?php
namespace workpackage\src\Traits\Http\Request;

use Closure;
use ReflectionMethod;
use ReflectionParameter;
use think\Facade;
use workpackage\src\Exceptions\Http\ProxyException;
use workpackage\src\Interfaces\Common\Parameters as ParametersInterface;
use workpackage\src\Libs\Common\Parameters;
use workpackage\src\Libs\Http\Request\ProxyLogic;

trait Proyx{
    
    protected $cacheProxyLogic = [];

    protected $cacheObjCall = [];

    /**
     * @param $handler
     * @return bool
     */
    public function isCallBack($handler) : bool {
        return !is_null(static::getCallable($handler));
    }

    /**
     * @param array|callable|\Closure|string $handler
     * @return callable|null
     */

    public static function getCallable($handler) {
        if (is_string($handler) && strpos($handler, '::') !== false) {
            // 拆分数组
            list($className, $method) = explode('::', $handler, 2);
            // 如果是可以调用的回调
            if (is_callable([$className, $method]) || method_exists($className, $method)) {
                // 重写回调
                $handler = [$className, $method];
            }
            // 释放内存
            unset($className, $method);
        }
        if (!is_callable($handler) && !method_exists($handler[0], $handler[1])) {
            // 重写回调
            $handler = null;
        }

        return $handler;
    }


    /**
     * 调用函数
     * @param array|callable|\Closure|string $handler
     * @param rray|\Easyke\Easycms\Interfaces\Common\Parameters $arguments
     * @param \workpackage\src\Interfaces\Common\Parameters $parameters
     * @param string|null $callCalssName
     * @return mixed
     * @throws ProxyException
     * @throws \ReflectionException
     */
    public function call($handler, $arguments = null, $parameters = null, $callCalssName = null){
        try{
            /**
             * 字符串强转为数组
             * 'AA\BB::CC' => ['AA\BB','CC']
             */
            $handler = self::getCallable($handler);

            /**
             * 试图调用类名
             */
            $className = is_array($handler) && isset($handler[0]) ? $handler[0] : null;

            /**
             * 判断类名是否存在
             */
            if($className && is_string($className) && class_exists($className)){
                /**
                 * 调用反射
                 */
                $reflectionMethod = new ReflectionMethod($className , $handler[1]);

                /**
                 * 判断是否为静态方法
                 * 如果是静态方法则不需要实例化
                 */
                if(!$reflectionMethod->isStatic()){
                    if (!(isset($this->cacheObjCall[$className]) && $this->cacheObjCall[$className] instanceof $className)) {
                        $carguments = [];
                        $cparameters = new Parameters([$className , '__construct'] , $carguments);
                        $this->argumentsInitByReflectionParameter($carguments , $cparameters);
                        $this->cacheObjCall[$className] = new $className(...$cparameters->getParameters());
                        unset($carguments , $cparameters);
                    }
                    //实例化
                    $handler = [$this->cacheObjCall[$className] , $handler[1]];
                }
                unset($reflectionMethod);
            }
            /**
             * 调用类的作用域
             * $var string $newClassScope
             */
            $newClassScope = empty($callCalssName) ? get_called_class() : $callCalssName ;



            /**
             * 如果第二个参数传入了第三个参数的内容，则交换内容
             */

            if(is_object($arguments) && $arguments instanceof ParametersInterface){
                $tmp = &$parameters;
                unset($parameters);
                $parameters = $arguments;
                unset($arguments);
                if(is_array($tmp)){
                    $arguments = &$tmp;
                }else{
                    $arguments = $parameters->getParameters();
                }
                unset($tmp);
            }

            /**
             * 确保第二个参数是一个数组
             */
            if(!is_array($arguments)){
                $arguments = [];
            }

            /**
             * 必须有一个解析参数的参数
             */
            if(!(isset($parameters) && $parameters instanceof ParametersInterface)){
                $parameters = new Parameters($handler , $arguments);
            }
            $this->argumentsInitByReflectionParameter($arguments , $parameters);
            
            return call_user_func_array(
                Closure::bind(
                    function ($handler, $arguments) {
                        return call_user_func_array($handler, $arguments);
                    },
                    null,
                    $newClassScope),
                [$handler, $arguments]
            );

        }catch (\ReflectionException $exception){
            throw new ProxyException(ProxyException::E_REFLECTION_EXCEPTION, [
                'message' => $exception->getMessage()
            ], $exception);
        }

    }

    /**
     * @param $callClass
     * @return mixed
     */
    public function getProxyLogicByCallClass($callClass){
        if(!(isset( $this->cacheProxyLogic[$callClass] ) && $this->cacheProxyLogic[$callClass] instanceof ProxyLogic )){
            $this->cacheProxyLogic[$callClass] = new ProxyLogic($callClass , $this);
        }
        return $this->cacheProxyLogic[$callClass];
    }

    /**
     * 清空
     */
    public function destroy(){
        $this->cacheProxyLogic = $this->cacheObjCall = [];
    }


    /**
     * @param $arguments
     * @param ParametersInterface $parameters
     */
    protected function argumentsInitByReflectionParameter(&$arguments, ParametersInterface $parameters){
        /**
         * 请求输入数据
         * @var array $input
         */
        $input = $this->param();
        /**
         * 遍历空参数，试图从请求输入中获取
         */
        $parameters->mapNullParameters(function(&$value, $name, $index, ReflectionParameter $parameter) use(&$input){
            $isHas = false;
            if(!empty($name)){
                if($this->attributes->has($name)){
                    //试图获取输入值
                    $data = $this->attributes->get($name);
                    //反射类
                    $reflectionClass = $parameter->getClass();

                    if ($parameter->isArray()){
                        //传入的值要求是数组，所以强转数组
                        $value = is_array($data) ? $data : [$data];
                        $isHas = true;
                    }elseif (empty($reflectionClass)){
                        //不转换
                        $value = $data;
                        $isHas = true;
                    }elseif (is_object($data) && $reflectionClass->isInstance($data)){
                        $value = $data;
                        $isHas = true;
                    }
                    unset($data);
                }elseif (isset($input[$name])){
                    //试图获取输入值
                    $data = &$input[$name];
                    //反射类
                    $reflectionClass = $parameter->getClass();

                    if($parameter->isArray()){
                        // 传入的值要求是数组，所以强转数组
                        $value = is_array($data) ? $data : array($data);
                        $isHas = true;
                    } elseif (empty($reflectionClass)) {
                        // 不转换
                        $value = $data;
                        $isHas = true;
                    }
                    unset($data);
                }
            }

            unset($name, $value, $input, $parameter);
            return $isHas;
        });
        unset($input);


        /**
         * 通过反射类
         * 类名获取
         */
        $parameters->mapNullParameters(function (&$value , $name , $index , ReflectionParameter $parameter){
            // 反射类
            $reflectionClass = $parameter->getClass();
            if(!empty($reflectionClass)){
                /**
                 * 判断期待的类是否是 代理本身
                 */
                if($reflectionClass->isInstance($this)){
                    //返回代理
                    $value = $this;
                }elseif(!$parameter->isOptional()){
                    $value = app($reflectionClass->getName());
                }
            }
            unset($value , $name , $index , $parameter);
            return !empty($reflectionClass);
        });

        /**
         * 赋予默认值
         */
        $parameters->mapNullParameters(function(&$value , $name , $index , ReflectionParameter $parameter){

            if ($parameter->isDefaultValueAvailable()){
                $value = $parameter->getDefaultValue();
            }
            unset($name , $value , $index , $parameter);
        });

        /**
         * 通过检测
         */
        $parameters->mapParameters(function (&$value , $name , $index , ReflectionParameter $parameter){

            $className = $parameter->getClass();


            if(empty($className)){
                if(method_exists($parameter , 'hasType') && method_exists($parameter , 'getType')){
                    $type = $parameter->getType();
                    if ($parameter->hasType() && $type->isBuiltin()){
                        switch ($type->getName()){
                            case 'int':
                                $value = is_numeric($value) ? (int) $value : 0;
                                break;
                            case 'float':
                                $value = is_numeric($value) ? (float) $value : 0;
                                break;
                            case 'bool':
                                $value = (bool)$value;
                                break;
                            case 'string':
                                $value = (is_string($value) || is_numeric($value)) ? (string)$value : '';
                                break;
                            case 'array':
                                $value = isset($value) ? ( is_array($value) ? $value : [$value] ) : [];
                                break;
                        }
                    }
                }
            }
        });

        $arguments = $parameters->getParameters();
        unset($arguments, $parameters);
    }



}