<?php
namespace workpackage\src\Traits\Http\Request;
trait Proper
{


    /**
     * 获取一个属性
     * @param null $key
     * @return mixed
     */
    public function getProp($key = null , $value = null)
    {
        if (empty($key)) {
            throw new RequestException(RequestException::E_MUST_PARAM_KEY, ['key'=>'$key']);
        }
        if ($this->has($key)) {
            return $this->param($key);
        } else {
            if (func_num_args() > 1) {
                return $defaultValue;
            } else {
//                throw new RequestException(RequestException::E_PROTOTYPE_NOT_DEFINED, ['key'=>$key]);
            }
        }
    }

    /**
     * 设置属性
     * @param null $key
     * @param null $value
     * @return mixed
     */
    public function setProp($key = null , $value = null){
        $this->__set($key , $value);
        return $this;
    }

    /**
     * 复制属性
     * @param null $key
     * @param null $newKay
     * @return mixed
     */
    public function copyProp($key = null , $newKay = null){
        
    }

    /**
     * 判断属性是否存在
     * @param $key
     * @return mixed
     */
    public function hasProp($key) : bool {
        $bool = (bool)$this->has($key);
        return $bool;
    }

    /**
     * 批量设置属性
     * @param array $dataSet
     * @param bool $isReplace
     * @return mixed
     */
    public function loadProp(array $data , bool $isReplace = false){
        if(!empty($data)){
            foreach ($data as $key => $value){
                $this->setProp($key , $value);
            }
        }
        return $this;

    }

}