<?php
namespace workpackage\src\Traits\Http\Request;
use think\Validate as ThinkValidate;
use workpackage\src\Exceptions\Validate\ValidateException;

trait Validate{

    /**
     * 验证
     * @param array $validateData
     * @throws ValidateException
     * @throws \ReflectionException
     */
    public function getInputValidate(array $validateData){
        $validateModel = new ThinkValidate();
        $validateModel->rule($validateData);
        $data = [];
        if(!empty($validateData)){
            foreach ($validateData as $key => $value){
                $data[$key] = $this->getAttrbute($key) ? $this->getAttrbute($key) : $this->param($key);
            }
        }
        if(!$validateModel->check($data)){
            throw new ValidateException(ValidateException::E_VALIDATE_ERROR ,['error' => $validateModel->getError() ]);
        }
    }

}