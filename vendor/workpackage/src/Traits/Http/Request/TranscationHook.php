<?php
namespace workpackage\src\Traits\Http\Request;


trait TranscationHook{

    /**
     * 存储提交事务的钩子
     * @var array
     */
    protected $commitHooks = [];

    /**
     * 存储回滚事务的钩子
     * @var array
     */
    protected $rollBackHooks = [];

    /**
     * 存储完成事务的钩子
     * @var array
     */
    protected $completedHooks = [];

    /**
     * 监听提交
     * @param $handler
     * @return mixed
     */
    public function Commit($handler){
        $this->commitHooks[] = $handler;
        return $this;
    }

    /**
     * 监听结束
     * @param $handler
     * @return mixed
     */
    public function RollBack($handler){
        $this->rollBackHooks[] = $handler;
        return $this;
    }

    /**
     * 监听 Commit 或者 RollBack 之后触发
     * @param $handler
     * @return mixed
     */
    public function Completed($handler){
        $this->completedHooks[] = $handler;
        return $this;
    }

    /**
     * 清空所有事务的钩子
     * @return mixed
     */
    public function clearTranscationHooks(){
        $this->commitHooks = $this->rollBackHooks = $this->completedHooks = [];
    }


}