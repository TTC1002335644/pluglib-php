<?php
namespace workpackage\src\Traits\Http\Request;

use think\Model;
use think\db\Connection;
use ReflectionParameter;
use workpackage\src\Interfaces\Http\Request\NextTransaction;
use workpackage\src\Libs\Common\Parameters;
use workpackage\src\Libs\Http\Request\NextTranscation;

trait TranscationQuque
{
    use TranscationHook;

    /**
     * 默认完成事务之后清除所有的钩子
     * @var bool
     */
    protected $isClearHooksBeginAfter= true;

    /**
     * 默认事务完成之前需要清空所有的钩子
     * @var bool
     */
    protected $isClearHooksBeginBefore = true;

    /**
     * 事务队列
     * @var bool
     */
    protected $transcationQuque = [];

    /**
     * 运行事务的索引
     * @var int
     */
    protected $runQuqueIndex = -1;

    /**
     * 是否在运行事务
     * @var bool
     */
    protected $isRunTranscationQuque = false;

    /**
     * 事务的链接
     * @var array
     */
    protected $transcationConnections = [];


    /**
     * 插入事务队列
     * @param callable|array|string|\Closure $transaction 队列事物
     * @param callable|array|string|\Closure $ifCallable 队列是否启用
     * @return $this|static
     */
    public function pushTranscation($transcation , $ifCallable = null){
        $this->transcationQuque[] = [$transcation , $ifCallable];
        return $this;
    }

    /**
     * 开始事务
     * @param null $data
     * @throws \Throwable
     * @return $this
     */
    public function beginTranscation(){
        //开始前清空所有的钩子
        if($this->isClearHooksBeginBefore){
            $this->clearTranscationHooks();
        }
        //清空事务链接
        $this->transcationConnections = [];
        //标记正在运行的事务
        $this->isRunTranscationQuque = true;
        //错误记录
        $exceptions = [];


        try{
            //设置开始的索引
            $this->runQuqueIndex = 0;
            //试图运行最后一个事务
            $this->callTranscationQuque();
        }catch (\Exception $exception){
            //保存异常
            $exceptions[] = $exception;
        }catch (\Throwable $exception){
            //保存异常
            $exceptions[] = $exception;
        }


        //有异常就回滚事务
        foreach ($this->transcationConnections as $connection){
            empty($exceptions) ? $connection->commit() : $connection->rollback();
        }


        if(empty($exceptions)){
            /**
             * 遍历提交事务
             */
            foreach ($this->commitHooks as $handler){
                try{
                    $this->call($handler);
                }catch (\Throwable $e){
                    $exceptions[] = $e;
                }
            }

        }else{
            /**
             * 遍历回滚事务
             * */
            foreach ($this->rollBackHooks as $handler){
                try{
                    $arguments = [];
                    $parameters = new Parameters($handler , $arguments);
                    $parameters->mapNullParameters(function (&$value , $name , $index , ReflectionParameter $parameter) use (&$exceptions){
                        $class = $parameter->getClass();
                        $isHas = false;
                        if(!empty($class)){
                            foreach ($exceptions as $exception) {
                                if($class->isInstance($exception)){
                                    //返回控制器
                                    $value = $exception;
                                    $isHas = true;
                                    break;
                                }
                            }
                        }
                        // 释放内存
                        unset($class, $value, $name, $index, $parameter, $exceptions);
                        return $isHas;
                    });

                }catch (\Throwable $e){
                    // 把最新异常，插入的异常数组的最前面
                    array_unshift($exceptions, $e);

                }
            }

        }

        /**
         * 处理完成的事件
         */
        foreach ($this->completedHooks as $handler){

            try{
                $this->call($handler);

            }catch (\Throwable $exception){
                $exceptions[] = $exception;

            }
        }

        //标记
        $this->runQuqueIndex = 0;
        //标记正在运行的事务
        $this->isRunTranscationQuque = false;
        //开启事务前清空钩子
        if($this->isClearHooksBeginAfter){
            $this->clearTranscationHooks();
        }


        // 清空队列
        $this->transcationQuque = [];
        // 清空事物链接
        $this->transcationConnections = [];

        /**
         * 如果存在异常就抛出异常
         */
        if (!empty($exceptions)) {
            throw $exceptions[0];
        }
        // 返回本身，链式调用
        return $this;
    }

    /**
     * 设置需要开启事务的model
     * @param Model[]|Model|array|string $model
     * @return $this
     * @throws \Exception
     */
    public function pushTranscationModel($model){
        if(!$this->isRunTranscationQuque){
            //没有开启事务，直接返回
            return $this;
        }

        if(is_array($model)){
            foreach ($model as $v){
                $this->pushTranscationModel($v);
            }
            return $this;
        }elseif(is_object($model)){
            if($model instanceof Model){
                $connection = $model->getConnection();
            }else{
                throw new ProxyException('must extends abstract Model', 'MUST_EXTENDS_ABSTRACT_MODEL');
            }
        }elseif (is_string($model) && class_exists($model) && method_exists($model , 'getConnection')){
            $connection = (new $model())->getConnection();
        }

        if(is_object($connection) && method_exists($connection , 'startTrans')){
            if(!in_array($connection , $this->transcationConnections , true)){
                $connection->startTrans();
                $this->transcationConnections[] = $connection;
            }
            return $this;
        }else{
            throw new ProxyException('Invalid connection', 'INVALID_CONNECTION');
        }
    }


    /**
     * 清除事务队列
     * @throws \ReflectionException
     */
    protected function callTranscationQuque(){

        /**
         * 提前第 {$this->runQuqueIndex} 个事务
         */
        if($this->runQuqueIndex < count($this->transcationQuque) && isset($this->transcationQuque[$this->runQuqueIndex])){
            /**
             * 获取事务监听器
             */
            list($handler , $ifCallable) = $this->transcationQuque[$this->runQuqueIndex];
            $nextTranscation = new NextTranscation(function(){
                if($this->runQuqueIndex < count($this->transcationQuque)){
                    $this->runQuqueIndex ++ ;
                    $this->callTranscationQuque();
                    return true;
                }else{
                    return false;
                }
            });

            if(!empty($ifCallable)){
                if(false === $this->call($ifCallable)){
                    $nextTranscation();
                    return;
                }
            }

            /**
             * 标记参数中 是否 存在下一个事务的参数
             */
            $isUseNextTranscation = false;

            $arguments = [];

            $parameters = new Parameters($handler , $arguments);

            $parameters->mapNullParameters(function (&$value, $name, $index, ReflectionParameter $parameter) use  (&$nextTranscation, &$isUseNextTranscation) {
                // 参数中填充 上一个事务
                $isHas = false;
                $reflectionClass = $parameter->getClass();
                if (!empty($reflectionClass)) {
                    if ($reflectionClass->implementsInterface(NextTransaction::class) || $reflectionClass->isInstance($nextTranscation)) {
                        // 返回控制器
                        $value = $nextTranscation;
                        $isUseNextTranscation = $isHas = true;
                    }
                }
                // 释放内存
                unset($value, $name, $index, $parameter, $reflectionClass, $nextTranscation, $isUseNextTranscation);
                return $isHas;
            });
            /**
             * 调用当前事务
             */

            $this->call($handler, $parameters);

            if ($isUseNextTranscation === false){
                $nextTranscation();
            }

            // 释放内存
            unset($handler, $ifCallable, $nextTranscation);
        }
        // 释放内存
        unset($index);
    }



}