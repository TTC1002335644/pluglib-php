<?php
namespace workpackage\src\Traits\Http\Request;

use think\Collection;
use think\facade\Config;
use think\Request;
use think\Response as HttpResponse;
use think\exception\HttpResponseException;

//use think\response\Json;


trait Response{

    /**
     * @var
     */
    protected $response;

    /**
     * @var array
     */
    protected $responses = [];

    /**
     * 默认不使用
     * @var bool
     */
    protected $isUseResponse = false;


    /**
     * 错误码
     * @var int
     */
    public static $errorCode = 0;

    /**
     * 成功码
     * @var int
     */
    public static $successCode = 1;

    /**
     * @return bool
     */
    public function isUseResponse() : bool {

        return $this->isUseResponse;
    }

    /**
     * 输出
     * @param int $status
     * @param string $msg
     * @param array $data
     * @param int|null $statusCode
     * @param array $header
     */
    public function outPutResponse($status = 1 , $msg = '', $data = [] , ?int $statusCode = 200 , array $header = [] , $isSuccess = true){
        $result = [
            'status' => $status,
            'msg' => $msg,
            'code' => $statusCode
//            'data' => $data,
        ];
        $setResponseData = $this->getResponse();
        if(!empty($setResponseData)){
            $result = array_merge($result , $setResponseData);
        }
        $type = self::getResponseType();
        $response =  HttpResponse::create($result , $type , $statusCode ,$header);
        if($isSuccess){
            return $response->send();
        }else{
            throw new HttpResponseException($response);
        }
    }


    /**
     * 开启swoole-http进程时，返回的请求
     * @param int $status
     * @param string $msg
     * @param array $data
     * @param int|null $statusCode
     * @param array $header
     * @return HttpResponse
     */
    public function outPutResponseSwoole($status = 1 , $msg = '', $data = [] , ?int $statusCode = 200 , array $header = []){
        $result = [
            'status' => $status,
            'msg' => $msg,
            'code' => $statusCode
//            'data' => $data,
        ];
        $setResponseData = $this->getResponse();
        if(!empty($setResponseData)){
            $result = array_merge($result , $setResponseData);
        }
        $type = self::getResponseType();
        return HttpResponse::create($result , $type , $statusCode ,$header)->send();
    }




    /**
     * 成功返回的Response
     * @param $status
     * @param $msg
     * @param $data
     */
    public function ResponseSuccess(int $status , string $msg = '' , $data , ?int $statusCode = 200 ){
        $header['Access-Control-Allow-Origin']  = '*';
        $header['Access-Control-Allow-Headers'] = 'X-Requested-With,Content-Type';
        $header['Access-Control-Allow-Methods'] = 'GET,POST,PATCH,PUT,DELETE,OPTIONS';
        return $this->outPutResponseSwoole($status , $msg , $data , $statusCode ,$header);
    }



    /**
     * 错误时返回的Response
     * @param int $status
     * @param $msg
     * @param $data
     * @param int|null $statusCode
     */
    public function ResponseError(int $status , string $msg = '' , $data ,  ?int $statusCode = 500){
        return $this->outPutResponse($status , $msg , $data , $statusCode , [] ,false );
    }

    /**
     * @param string $msg
     * @param int $status
     * @param $data
     * @param int|null $statusCode
     */
    public function ResponseReturn(int $status = 1 , string $msg = '成功'  , ?array $data = [] , ? int $statusCode = 200){
        return $this->ResponseSuccess($status , $msg , $data , $statusCode);
    }

    /**
     * 成功返回的Response
     * @return mixed
     */
    public static function getResponseType(){
        return Config::get('default_ajax_return');
    }

    /**
     * 设置多个response
     * @param array $data
     * @return $this
     */
    public function setResponses($data){
        if(empty($data)){
            return $this;
        }
        if(is_object($data)){
            $data = $data->toArray();
        }
        foreach ($data as $k => $v){
            $this->setResponse($k,$v);
        }
        return $this;
    }


    /**
     * @param $key
     * @param $value
     */
    public  function setResponse($key, $value){
        $this->isUseResponse = true;
        if($key === static::RESPONSES_ARRAY_MERGE){
            //如果key为false，则重写所有的输出
            if(is_array($value)){
                $this->responses = array_merge($this->responses , $value);
            }else{
                $this->responses = $value ;
            }
        }elseif ($key === static::RESPONSES_REPLACE){
            // 如果$key为true就是需要强制重写所有输出
            $this->responses = $value ;

        }elseif ( isset($key) && (is_string($key) || is_numeric($key))){
            $key = (string) $key;
            $tmp = &$this->responses;
            if(!is_array($tmp)){
                $tmp = [];
            }
            foreach (explode('.',$key) as $name){
                if(!empty($name)){
                    if(!isset($tmp[$name])){
                        $tmp[$name] = [];
                    }
                    $tmp = &$tmp[$name];
                }
            }
            //把数据保存起来
            $tmp = $value;
            unset($tmp);
        }

        if(isset($value) && is_object($value) && $value instanceof Collection){
            //这里是搞页码
        }

        //释放内存
        unset($key , $value);
        return $this;
    }


    /**
     * 获取请求参数
     * @param null $request
     * @return array
     */
    public function getResponse($request = null){
        return $this->responses;
    }
    /**
     * @return $this
     */
    public function clearResponse(){
        $this->responses = null;
        $this->isUseResponse = false;
        return $this;
    }




}