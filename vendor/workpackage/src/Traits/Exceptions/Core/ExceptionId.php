<?php

namespace workpackage\src\Traits\Exceptions\Core;

use workpackage\src\Traits\Http\Request\Response;


/**
 * Trait ExceptionId
 * @package workpackage\src\Traits\Exceptions\Core
 */
trait ExceptionId{

    use Response;


    /**
     * 处理字符串
     * @param int $code
     * @param null $message
     * @return string
     */
    public function transformMessage(int $code = 0 , $message = null ) : string {
        $messageArrDir = $this->messageArrDir;
        //获取异常数组
        $messageArr = $messageArrDir::MESSAGES;
        $str = '';//返回的字符串
        if(!empty($message) && is_array($message) && !empty($messageArr)){
            $str = $messageArr[$code];
            foreach ($message as $key => $value){
                $str = str_replace('{'.$key.'}' , $value , $str);
            }

        }
        return $str;
    }




}