<?php
namespace workpackage\src\Exceptions\Validate;

use workpackage\src\Exceptions\Base\BaseException;
use workpackage\src\Interfaces\Exceptions\ExceptionArray\Validate\ValidateLists;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Validate\Validate;

class ValidateException extends BaseException implements Validate
{
    protected $MSG_ARR_DIR = ValidateLists::class;

}