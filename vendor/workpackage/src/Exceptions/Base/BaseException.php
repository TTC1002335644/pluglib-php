<?php
namespace workpackage\src\Exceptions\Base;

use Throwable;
use workpackage\src\Traits\Exceptions\Core\ExceptionId;
use workpackage\src\Traits\Exceptions\Core\Http;
use workpackage\src\Traits\Exceptions\CoreTraitsException;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Common\Common;

class BaseException  extends \Exception implements Common{

    /**
     * 默认状态码
     */
    const STATUS_CODE = 400;
    /**
     * 默认异常码
     */
    const EXCEPTION_DEFAULT_CODE = 50000;

    use CoreTraitsException;


}