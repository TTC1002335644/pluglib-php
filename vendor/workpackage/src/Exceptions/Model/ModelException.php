<?php
namespace workpackage\src\Exceptions\Model;

use workpackage\src\Exceptions\Base\BaseException;
use workpackage\src\Interfaces\Exceptions\ExceptionArray\Model\ModelLists;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Model\Model;

class ModelException extends BaseException implements Model {

    protected $MSG_ARR_DIR = ModelLists::class;


}