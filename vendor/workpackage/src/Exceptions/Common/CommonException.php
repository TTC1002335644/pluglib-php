<?php
namespace workpackage\src\Exceptions\Common;


use workpackage\src\Exceptions\Base\BaseException;
use workpackage\src\Interfaces\Exceptions\ExceptionArray\Common\CommonLists;
use workpackage\src\Interfaces\Exceptions\ExceptionGroup\Common\Common;

class CommonException extends BaseException implements Common
{

    protected $MSG_ARR_DIR = CommonLists::class;


}