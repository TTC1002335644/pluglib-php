<?php
namespace think\helper;

/**
 * Class Arr
 * @package think\helper
 */
class Arr
{

    public static function isAssoc(array $array)
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }

    public static function sortRecursive($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = static::sortRecursive($value);
            }
        }

        if (static::isAssoc($array)) {
            ksort($array);
        } else {
            sort($array);
        }

        return $array;
    }


    /**
     * 所有索引转驼峰
     * @param array $input
     * @return array
     */
    public static function indexCamel(array $input = []) : array {
        $res = [];
        foreach ($input as $key => $value) {
            $res[Str::camel($key)] = is_array($value) ? static::indexCamelRecursive($value) : $value;
        }
        unset($input);
        return $res;
    }

    /**
     * 所有索引由驼峰转下划线
     * @param array $input
     * @return array
     */
    public static function indexSnake(array $input): array {
        $res = [];
        foreach ($input as $key => $value) {
            $res[Str::snake($key)] = $value;
        }
        unset($input);
        return $res;
    }


    /**
     * 数组中所有的值都转下划线
     * @param array $input
     * @return array
     */
    public static function arraySnake(array $input) :array {
        $input = array_map(function ($v){
            return Str::snake($v);
        } , $input);
        return $input;
    }


    /**
     * 比较两个数组，并返回交集
     * @param $array
     * @param $keys
     * @return array
     */
    public static function only($array, $keys){
        return array_intersect_key($array, array_flip((array) $keys));
    }


    /**
     * 所有索引转驼峰
     * @param array $input
     * @return array
     */
    public static function indexCamelRecursive(array $input): array
    {
        $res = [];
        foreach ($input as $key => $value) {
            $res[Str::camel($key)] = is_array($value) ? static::indexCamelRecursive($value) : $value;
        }
        unset($input);
        return $res;
    }





}