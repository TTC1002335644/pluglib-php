<?php
/**
 * Created by PhpStorm.
 * User: 10023
 * Date: 2019/4/21
 * Time: 23:25
 */

namespace app\Http\Model\Admin\auth;


use workpackage\src\Model\BaseModel;

class AdminModel extends BaseModel
{

    protected $name = 'admin';

    protected $pk = 'admin_id';


    /**
     * 管理员状态
     */
    const STATUS_ON = 1;//正常

    const STATUS_OFF = 2;//禁用

    /**
     * @return \think\model\relation\HasMany
     */
    public function hasManyGroup(){
        return $this->hasMany(GroupAccessModel::class , 'uid' , 'admin_id')->with([
            'hasOneGroup' => function($query){
                $query->field(['groupId' , 'pid' , 'name' ,'status']);
            }
        ]);

    }

}