<?php
namespace app\Http\Model\Admin\auth;

use workpackage\src\Model\BaseModel;

class GroupAccessModel extends BaseModel{

    protected $name = 'auth_group_access';

    /**
     * @return \think\model\relation\HasOne
     */
    public function hasOneGroup(){
        return $this->hasOne(GroupModel::class , 'group_id' , 'group_id');
    }


}