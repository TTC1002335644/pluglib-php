<?php
namespace app\Http\Model\Admin\auth;

use workpackage\src\Model\BaseModel;

/**
 * Class AgeModel
 * @package app\Http\Model\Admin\auth
 */
class AgeModel extends BaseModel
{

    protected $name = 'age';

}