<?php
namespace app\Http\Model\Admin\auth;


use workpackage\src\Model\BaseModel;

class GroupModel extends BaseModel{

    protected $name = 'auth_group';

    protected $pk = 'group_id';

    /**
     * 管理员状态
     */
    const STATUS_ON = 1;//正常

    const STATUS_OFF = 2;//禁用


}