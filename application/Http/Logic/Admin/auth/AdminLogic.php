<?php

namespace app\Http\Logic\Admin\auth;


use app\Http\Model\Admin\auth\AdminModel;
use app\Http\Model\Admin\auth\AgeModel;
use app\Http\Model\Admin\auth\TestModel;
use workpackage\src\Exceptions\Common\CommonException;
use workpackage\src\Exceptions\Model\ModelException;
use workpackage\src\Libs\Http\Request;
use workpackage\src\Logic\BaseLogic;

class AdminLogic extends BaseLogic {


    public function duilie(Request $request){
        $data = [
            'id' => 6,
        ];
//        $model = TestModel::pushQuqueTransaction($request)->where($data)->select()->toArray();
//        $request->setResponse('lists' , $model);
        $request->setAttribute('id',123);
    }

    public function duilie3(int $page = 1 , int $len = 10 , string $order='adminId' , string $sort = 'ASC' ,Request $request){
        $request->getInputValidate([
            'page' => 'integer',
            'len' => 'integer',
            'sort' => 'in:asc,desc,ASC,DESC',
        ]);

        $list = AdminModel::pushQuqueTransaction($request)
            ->field(['password' , 'salt' , 'loginfailure' , 'token'],true)
            ->with([
                'hasManyGroup'
            ])
            ->page($page , $len)
            ->order($order , $sort)
            ->getListsPage();
        $request->setResponses($list);

    }



    public function testAttr1(Request $request){
        $data = [
            'name' => 'ttc',
            'age' => 24
        ];


        $request->setAttributes($data);

        $request->loadProp($data);
        $request->setProp('id',123);
    }
    public function testAttr2(Request $request){
        dump($request->getAttrbute('name'));
        dump($request->getAttrbute('age'));

        dump($request->param());

//        dump(array_only($request->param() , ['name' , 'id']));
    }


    public function testVali(Request $request){
        $request->getInputValidate([
            'id' => 'require|integer',
            'name' => 'require|min:4',
            'age' => 'require|integer',
            'weight' => 'require|integer',
        ]);
    }












}