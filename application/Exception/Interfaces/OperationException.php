<?php
namespace app\Exception\Interfaces;


interface OperationException{

    /**
     * @example {message}保存失败
     */
    const E_ERROR_SAVE_FAILE = 700001;


    /**
     * @example {message}删除失败
     */
    const E_ERROR_DELETE_FAILE = 700002;


    /**
     * @example {message}修改状态失败
     */
    const E_ERROR_UPDATE_STATUS_FAILE = 700003;




}