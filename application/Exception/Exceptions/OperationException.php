<?php
namespace app\Exception\Exceptions;


use app\Exception\MessageLists\OperationLists;
use workpackage\src\Exceptions\Base\BaseException;

class OperationException extends BaseException implements \app\Exception\Interfaces\OperationException
{
    protected $MSG_ARR_DIR = OperationLists::class;

}