<?php
namespace app\Exception\MessageLists;
use app\Exception\Interfaces\OperationException;


class OperationLists{

    const MESSAGES = [
        OperationException::E_ERROR_SAVE_FAILE => '{message}保存失败',
        OperationException::E_ERROR_DELETE_FAILE => '{message}删除失败',
        OperationException::E_ERROR_UPDATE_STATUS_FAILE => '{message}修改状态失败',
    ];


}