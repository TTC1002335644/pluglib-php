<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/20
 * Time: 17:06
 * Author: ttc
 */

namespace app\Common\controller;


use think\Controller;

use app\Traits\Admin\BaseTrait;
use workpackage\src\Libs\Http\Request;

class BaseAdminController extends Controller
{
    use BaseTrait;

}