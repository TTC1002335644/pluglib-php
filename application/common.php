<?php
use think\helper\Arr;
use think\Loader;

Loader::addNamespace('package',Loader::getRootPath() . 'extend' . DIRECTORY_SEPARATOR);
Loader::addNamespace('workpackage',Loader::getRootPath() . 'vendor/workpackage' . DIRECTORY_SEPARATOR);
Loader::addNamespace('workTool',Loader::getRootPath() . 'vendor/workTool' . DIRECTORY_SEPARATOR);


/**
 * 返回两个数组的公有键的部分
 */
if(!function_exists('array_only')){
    function array_only($array , $key){
        return Arr::only($array , $key);
    }
}



// 应用公共文件
if(!function_exists('d')){
    function d($data){
        dump($data);
    }
}

if(!function_exists('de')){
    function de($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        echo "<br>";
    }
}
