<?php
namespace app\Route;
//use app\admin\controller\auth\Admin;
use app\Http\Controller\Admin\auth\Admin;
use package\Route\Route;
//use think\facade\Route;
final class AdminRoute
{

    public static function reg(){
        self::noPower();
    }

    /**
     * 无需权限验证的接口
     * */
    public static function noPower(){

        Route::get('hello1', [Admin::class , 'index']);

        Route::get('name', [Admin::class , 'index']);


        /**
         * 时间测试
         */
        Route::get('time', [Admin::class , 'time']);

        /**
         * 请求测试
         */
        Route::any('qingqiu',[Admin::class , 'qingqiu']);

        /**
         * 队列测试
         * */
        Route::any('duilie',[Admin::class , 'duilie']);
        Route::any('duilie2',[Admin::class , 'duilie2']);

        /**
         * 属性测试
         */
        Route::any('testAttr',[Admin::class , 'testAttr']);


        Route::any('testVali',[Admin::class , 'testVali']);
    }









}